<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"Category"})
     * @Expose()
     */
    private $Id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Category"})
     * @Expose()
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Slug(fields={"Name"})
     * @Groups({"Category"})
     * @Expose()
     */
    private $Slug;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"Category"})
     * @Expose()
     */
    private $Authorize = true;

    /**
     * @ORM\OneToMany(targetEntity=SubCategory::class, mappedBy="Category")
     * @Groups({"Sub_Category"})
     * @Expose()
     */
    private $SubCategories;

    public function __toString(){
        return $this->Name;
    }

    public function __construct()
    {
        $this->SubCategories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->Id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->Slug;
    }

    public function setSlug(string $Slug): self
    {
        $this->Slug = $Slug;

        return $this;
    }

    public function getAuthorize(): ?bool
    {
        return $this->Authorize;
    }

    public function setAuthorize(bool $Authorize): self
    {
        $this->Authorize = $Authorize;

        return $this;
    }

    /**
     * @return Collection|SubCategory[]
     */
    public function getSubCategories(): Collection
    {
        return $this->SubCategories;
    }

    public function addSubCategory(SubCategory $SubCategory): self
    {
        if (!$this->SubCategories->contains($SubCategory)) {
            $this->SubCategories[] = $SubCategory;
            $SubCategory->setCategory($this);
        }

        return $this;
    }

    public function removeSubCategory(SubCategory $SubCategory): self
    {
        if ($this->SubCategories->removeElement($SubCategory)) {
            // set the owning side to null (unless already changed)
            if ($SubCategory->getCategory() === $this) {
                $SubCategory->setCategory(null);
            }
        }

        return $this;
    }
}
