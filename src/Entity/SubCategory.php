<?php

namespace App\Entity;

use App\Repository\SubCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=SubCategoryRepository::class)
 */
class SubCategory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"Sub_Category"})
     * @Expose()
     */
    private $Id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Sub_Category"})
     * @Expose()
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Slug(fields={"Name"})
     * @Groups({"Sub_Category"})
     * @Expose()
     */
    private $Slug;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="SubCategories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Category;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="SubCategory")
     * @Groups({"Product"})
     * @Expose()
     */
    private $Products;

    public function __toString(){
        return $this->Name;
    }

    public function __construct()
    {
        $this->Products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->Id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $name): self
    {
        $this->Name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->Slug;
    }

    public function setSlug(string $slug): self
    {
        $this->Slug = $slug;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->Category;
    }

    public function setCategory(?Category $Category): self
    {
        $this->Category = $Category;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->Products;
    }

    public function addProduct(Product $Product): self
    {
        if (!$this->Products->contains($Product)) {
            $this->Products[] = $Product;
            $Product->setSubCategory($this);
        }

        return $this;
    }

    public function removeProduct(Product $Product): self
    {
        if ($this->Products->removeElement($Product)) {
            // set the owning side to null (unless already changed)
            if ($Product->getSubCategory() === $this) {
                $Product->setSubCategory(null);
            }
        }

        return $this;
    }
}
