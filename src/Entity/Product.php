<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @Vich\Uploadable
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"Product"})
     * @Expose()
     */
    private $Id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Product"})
     * @Expose()
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Product"})
     * @Expose()
     */
    private $PriceCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"Product"})
     * @Expose()
     */
    private $ImageName;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="ImageName")
     * @var File
     */
    private $ImageFile;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity=SubCategory::class, inversedBy="Products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $SubCategory;

    public function getId(): ?int
    {
        return $this->Id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getPriceCode(): ?string
    {
        return $this->PriceCode;
    }

    public function setPriceCode(string $PriceCode): self
    {
        $this->PriceCode = $PriceCode;

        return $this;
    }

    public function setImageFile(File $Image = null)
    {
        $this->ImageFile = $Image;

        if ($Image) {
            $this->created = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->ImageFile;
    }

    public function getImageName(): ?string
    {
        return $this->ImageName;
    }

    public function setImageName(?string $ImageName): self
    {
        $this->ImageName = $ImageName;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getSubCategory(): ?SubCategory
    {
        return $this->SubCategory;
    }

    public function setSubCategory(?SubCategory $SubCategory): self
    {
        $this->SubCategory = $SubCategory;

        return $this;
    }
}
