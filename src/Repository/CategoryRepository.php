<?php

namespace App\Repository;

use App\Constant\APIConstants;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    // /**
    //  * @return Category[] Returns an array of Category objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function getCategory($PageIndex)
    {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->select('c');

        if ($PageIndex && $PageIndex > 1) {
            $qb->setFirstResult(($PageIndex - 1) * APIConstants::MAX_LIMIT);
        }

        $qb
            ->setMaxResults(APIConstants::MAX_LIMIT);
        return $qb->getQuery()->getResult();
    }

}
