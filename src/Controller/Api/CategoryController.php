<?php

namespace App\Controller\Api;

use App\Constant\APIConstants;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\SubCategory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CategoryController
 */
class CategoryController extends BaseController
{
    /**
     * get categories
     *
     * @Route("categories/", methods={"POST"})
     * @SWG\Response(
     *     response=200,
     *     description="get categories",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Category::class))
     *     )
     * )
     * @SWG\Parameter(
     *     name="PageIndex",
     *     in="formData",
     *     type="string",
     *     description="Page Index"
     * )
     * @SWG\Tag(name="Category")
     * @param Request $request
     * @return Response
     */
    public function getCategories(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $PageIndex = $request->get('PageIndex', APIConstants::DEFAULT_PAGE);
        $Categories = $em->getRepository(Category::class)->getCategory($PageIndex);

        return $this->success($Categories , ['Category', 'Sub_Category', 'Product']);
    }

    /**
     * get sub categories
     *
     * @Route("sub_categories/", methods={"POST"})
     * @SWG\Response(
     *     response=200,
     *     description="get sub categories",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=SubCategory::class))
     *     )
     * )
     * @SWG\Parameter(
     *     name="CategoryId",
     *     in="formData",
     *     type="string",
     *     description="Category Id"
     * )
     * @SWG\Parameter(
     *     name="PageIndex",
     *     in="formData",
     *     type="string",
     *     description="Page Index"
     * )
     * @SWG\Tag(name="Category")
     * @param Request $request
     * @return Response
     */
    public function getSubCategories(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $CategoryId = $request->get('CategoryId', null);
        $PageIndex = $request->get('PageIndex', APIConstants::DEFAULT_PAGE);
        $SubCategories = $em->getRepository(SubCategory::class)->getSubCategory($CategoryId, $PageIndex);

        return $this->success($SubCategories , ['Sub_Category', 'Product']);
    }

    /**
     * get products
     *
     * @Route("products/", methods={"POST"})
     * @SWG\Response(
     *     response=200,
     *     description="get products",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Product::class))
     *     )
     * )
     * @SWG\Parameter(
     *     name="SubCategoryId",
     *     in="formData",
     *     type="string",
     *     description="Category Id"
     * )
     * @SWG\Parameter(
     *     name="PageIndex",
     *     in="formData",
     *     type="string",
     *     description="Page Index"
     * )
     * @SWG\Tag(name="Category")
     * @param Request $request
     * @return Response
     */
    public function getProducts(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $SubCategoryId = $request->get('SubCategoryId', null);
        $PageIndex = $request->get('PageIndex', APIConstants::DEFAULT_PAGE);
        $SubCategories = $em->getRepository(Product::class)->getProducts($SubCategoryId, $PageIndex);

        return $this->success($SubCategories , ['Sub_Category', 'Product']);
    }

}
